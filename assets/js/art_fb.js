var form = layui.form
function artfb() {
    $.ajax({
        type: 'get',
        url: '/my/article/cates',
        success: function (res) {
            // console.log(res);
            var str = template('tpl-cate', res);
            $('#select').html(str);
            form.render()
        }
    })
};
artfb();
initEditor();
var $image = $('#image')

// 2. 裁剪选项
var options = {
    aspectRatio: 400 / 280,
    preview: '.img-preview'
};

// 3. 初始化裁剪区域
$image.cropper(options);
$('#btnChooseImage').click(function () {
    $('#coverFile').click();

});
$('#coverFile').change(function (e) {
    var file = this.files
    // console.log(file);
    var imgurl = URL.createObjectURL(file[0]);
    $image
        .cropper('destroy') // 销毁旧的裁剪区域
        .attr('src', imgurl) // 重新设置图片路径
        .cropper(options) // 重新初始化裁剪区域
});
var art_state = '已发布';
$('#btnSave2').on('click', function () {
    art_state = '草稿'
});
$('#form-pub').on('submit', function (e) {
    e.preventDefault();
    var fd = new FormData(this);
    console.log(this);
    fd.append('state', art_state);
    $image
        .cropper('getCroppedCanvas', {
            width: 400,
            height: 280
        }).toBlob(function (blob) {
            fd.append('cover_img', blob)
            xuanran1(fd)
        });
    // fd.forEach(function (v) {
    //     console.log(v);
    // })
    // console.log(fd);

});
function xuanran1(fd) {
    $.ajax({
        method: 'POST',
        url: '/my/article/add',
        data: fd,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.status !== 0) {
                return layer.msg('发布文章失败')
            }
            layer.msg('发布文章成功')
            window.parent.gl();
            location.href = '/article/art_list.html';
        }
    })
}

