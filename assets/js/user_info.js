var from = layui.form;
// var layer = layui.layer;
from.verify({
    nickname: function (value) {
        if (value.length > 6) {
            return '昵称长度必须在 1 ~ 6 个字符之间！'
        }
    }
})
function reset() {
    $.ajax({
        type: 'get',
        url: '/my/userinfo',
        success: function (res) {
            if (res.status == 0)
                console.log(res);
                from.val('formUserInfo', res.data);
        }

    })
}
reset();
$('#reset').click(function (e) {
    alert(1)
    e.preventDefault();
    reset();
})
$('.layui-form').submit(function (e) {
    // alert(1)
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: '/my/userinfo',
        data: $(this).serialize(),
        success: function (res) {
            console.log(res);
            if (res.status !== 0) return layer.msg('修改失败');
            window.parent.gets();
            layer.msg('修改成功');
        }
    })
})
