function gets() {
    $.ajax({
        type: 'get',
        url: '/my/userinfo',
        // headers: {
        //     Authorization: localStorage.getItem('token') || ''
        // },
        success: function (res) {
            if (res.status !== 0) return layer.msg(res.message);
            // console.log(res);
            renders(res.data);
        }
    })
}
gets();
function renders(data) {
    var val = data.nickname || data.username;
    $('#welcome').html('欢迎' + val);
    if (data.user_pic !== null) {
        $('.layui-nav-img').attr('src', data.user_pic).show();
        $('.text-avatar').hide();
    }
    else {
        var name = val[0].toUpperCase();
        $('.layui-nav-img').hide();
        $('.text-avatar').html(name).show();

    }
}
$('#logout').on('click', function () {
    8
    layer.confirm('确定退出登录?', { icon: 3, title: '提示' }, function (index) {
        localStorage.removeItem('token');
        location.href = "/login.html";
        layer.close(index);
    })
});
function gl() {
    $('#layui-this').addClass('layui-this').siblings().removeClass('layui-this')
}

