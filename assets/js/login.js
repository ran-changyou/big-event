$(function () {
  $('#link-reg').on('click', function () {
    $('.reg-box').show();
    $('.login-box').hide();
  });
  $('#link-login').on('click', function () {
    $('.login-box').show();
    $('.reg-box').hide();
  });
  var form = layui.form;
  form.verify({
    ps: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ],
    rePwd: function (value) {
      var rep = $('#psw').val();
      if (rep !== value) {
        return '两次输入的密码不一致'
      }
    }
  });
  var layer = layui.layer;
  $('#reg').submit(function (e) {
    e.preventDefault()
    var data = $(this).serialize();
    console.log(data);
    $.ajax({
      type: 'post',
      url: '/api/reguser',
      data,
      success: function (res) {
        if (res.status !== 0) return layer.msg(res.message);
        layer.msg('注册成功,请登入');
        $('#reg')[0].reset();
        $('#link-login').click()
      }
    })
  });
  $('#login').submit(function (e) {
    e.preventDefault();
    $.ajax({
      type: 'post',
      url: '/api/login',
      data: $(this).serialize(),
      success: function (res) {
        if (res.status !== 0) return layer.msg(res.message);
        console.log(res);
        layer.msg('登入成功');
        localStorage.setItem('token', res.token);
        location.href = '/index.html'
      }
    })
  })
})

