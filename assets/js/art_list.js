var a = {
    pagenum: 1,
    pagesize: 5,
    cate_id: '',
    state: '',
}
var layer = layui.layer;
var form = layui.form;
var laypage = layui.laypage;
function xuanran2() {
    $.ajax({
        type: 'get',
        url: '/my/article/list',
        data: a,
        success: function (res) {
            var str = template('tpl-table', res);
            $('tbody').html(str);
            xuanranfy(res.total)
        }
    })
}
xuanran2();
xuanran3()
template.defaults.imports.dataFormat = function (date) {
    const dt = new Date(date)

    var y = dt.getFullYear()
    var m = padZero(dt.getMonth() + 1)
    var d = padZero(dt.getDate())

    var hh = padZero(dt.getHours())
    var mm = padZero(dt.getMinutes())
    var ss = padZero(dt.getSeconds())

    return y + '-' + m + '-' + d + ' ' + hh + ':' + mm + ':' + ss
}

// 定义补零的函数
function padZero(n) {
    return n > 9 ? n : '0' + n
}
function xuanran3() {
    $.ajax({
        type: 'get',
        url: '/my/article/cates',
        success: function (res) {
            if (res.status !== 0) return layer.msg('获取失败');
            var str = template('tpl-cate', res);
            $('#cate_id').html(str);
            form.render()
        }
    })
}
$('#form-search').submit(function (e) {
    e.preventDefault();
    a.cate_id = $('#cate_id').val();
    console.log(a.cate_id);
    a.state = $('#state').val();
    xuanran2()

})
function xuanranfy(total) {
    // console.log(total);
    laypage.render({
        elem: 'pageBox', // 分页容器的 Id
        count: total, // 总数据条数
        limit: a.pagesize, // 每页显示几条数据
        curr: a.pagenum, // 设置默认被选中的分页
        layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
        limits: [2, 3, 5, 10],
        jump: function(obj, first) {
            // 可以通过 first 的值，来判断是通过哪种方式，触发的 jump 回调
            // 如果 first 的值为 true，证明是方式2触发的
            // 否则就是方式1触发的
            console.log(first)
            console.log(obj.curr)
            // 把最新的页码值，赋值到 a 这个查询参数对象中
            a.pagenum = obj.curr
            // 把最新的条目数，赋值到 a 这个查询参数对象的 pagesize 属性中
            a.pagesize = obj.limit
            // 根据最新的 a 获取对应的数据列表，并渲染表格
            if (!first) {
                xuanran2()
            }
          }
    })
}
