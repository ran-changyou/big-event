function xuanran() {
    $.ajax({
        type: 'get',
        url: '/my/article/cates',
        success: function (res) {
            // console.log(res);
            var vals = template('trs', res);
            $('tbody').html(vals)
        }
    })
}
xuanran();
var form = layui.form;
var layer = layui.layer;
var index = null;
$('#btnAddCate').on('click', function () {
    index = layer.open({
        type: 1,
        area: ['500px', '250px'],
        title: '添加文章分类',
        content: $('#trs1').html()
    })
})
$('body').on('submit', '#form-add', function (e) {
    // alert(1)
    e.preventDefault();
    $.ajax({
        method: 'post',
        url: '/my/article/addcates',
        data: $(this).serialize(),
        success: function (res) {
            if (res.status !== 0) return layer.msg('新增失败');
            xuanran();
            layer.msg('添加成功');
            console.log(res);
            layer.close(index);
        }
    })
})
var indexEdit = null
$('body').on('click', '.btn-edit', function () {
    // 弹出一个修改文章分类信息的层
    indexEdit = layer.open({
        type: 1,
        area: ['500px', '250px'],
        title: '修改文章分类',
        content: $('#dialog-edit').html()
    })
    var id = $(this).attr('data-id');
    $.ajax({
        method: 'GET',
        url: '/my/article/cates/' + id,
        success: function (res) {
            console.log(1);
            form.val('form-edit', res.data)
        }
    })
})
$('body').on('submit', '#form-edit', function (e) {
    e.preventDefault();
    $.ajax({
        method: 'POST',
        url: '/my/article/updatecate',
        data: $(this).serialize(),
        success: function (res) {
            if (res.status !== 0) return layer.msg('修改失败');
            layer.msg('修改成功');
            console.log(res);
            xuanran();
            layer.close(indexEdit)
        }
    })
})
$('body').on('click', '.btn-shanchu', function () {
    var id = $(this).attr('data-id');
    // console.log($(this));
    layer.confirm('确认删除?', { icon: 3, title: '提示' }, function (index) {
        $.ajax({
            type: 'get',
            url: '/my/article/deletecate/' + id,
            success: function (res) {
                if (res.status !== 0) return layer.msg('删除失败');
                layer.msg('删除成功');
                console.log(res);
                xuanran();
                layer.close(index)
            }
        })
    })

})